package edu.sfsu.cs.orange.ocr;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;
//import com.google.firebase.FirebaseApp;
//import com.google.firebase.database.DatabaseError;
//import com.google.firebase.database.DatabaseReference;
//import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import edu.sfsu.cs.orange.ocr.entidades.Carro;

/**
 * Created by jonatas on 19/06/18.
 */
public class Pesquisa extends AppCompatActivity {
   Firebase myRef = new Firebase("https://samu-192.firebaseio.com/veiculos");
    EditText pesquisa;
    ListView lista;
    String placa;



//    FirebaseDatabase database;
  //  DatabaseReference myRef;

    private List<Carro> CarroList = new ArrayList<Carro>();
    private ArrayAdapter<Carro> CarroArrayAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pesquisa);


        pesquisa=findViewById(R.id.pesquisa);
        lista=findViewById(R.id.lista2);


        Firebase.setAndroidContext(this);



        //  FirebaseApp.initializeApp(Pesquisa.this);
        //  database = FirebaseDatabase.getInstance();
        // database.setPersistenceEnabled(true);
        eventoEditar();


    }


        private void eventoEditar(){

        pesquisa.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String palavra = pesquisa.getText().toString().trim();
                pesquisarPalavra(palavra);

            }
        });

    }

    private void pesquisarPalavra(String palavra){

        Query query;
        if(palavra.equals("")){
            query = myRef.orderByChild("placa");
        }else{
            query = myRef.orderByChild("placa").startAt(palavra).endAt(palavra+"\uf8ff");
        }

        CarroList.clear();

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot objSnapshot : dataSnapshot.getChildren()) {
                    Carro p = objSnapshot.getValue(Carro.class);

                    CarroList.add(p);
                }
                CarroArrayAdapter = new ArrayAdapter<Carro>(Pesquisa.this,
                        android.R.layout.simple_list_item_1, CarroList);
                lista.setAdapter(CarroArrayAdapter);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {


            }

        });

    }



    @Override
    protected void onResume() {
        super.onResume();

            pesquisarPalavra("");

    }


}

