package edu.sfsu.cs.orange.ocr;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.firebase.database.DatabaseError;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import edu.sfsu.cs.orange.ocr.entidades.Carro;
//import edu.sfsu.cs.orange.ocr.entidades.Config;

public class Principal extends AppCompatActivity {

    EditText placa, cor, modelo, ano;

    ListView lista;

    Button btscanner;



    private List<Carro> CarroList = new ArrayList<Carro>();
    private ArrayAdapter<Carro> CarroArrayAdapter;

    Carro CarroSelecionada;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        placa = findViewById(R.id.editPlaca);
        cor= findViewById(R.id.editCor);
        modelo = findViewById(R.id.editModelo);
        ano = findViewById(R.id.editAno);

        btscanner = findViewById(R.id.bt2);

       lista = findViewById(R.id.listCarro);

        Firebase.setAndroidContext(this);
//        Firebase objFirebase = new Firebase("https://samu-192.firebaseio.com/veiculos");
//  Firebase objFirebase = new Firebase("https://samu-192.firebaseio.com/");
        btscanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(Principal.this,CaptureActivity.class);
                startActivity(intent);

            }
        });

        eventoDataBase();

        eventoClicar();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }


    //pegando a ação do menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();


        if (id== R.id.adicionar){
            Firebase objFirebase = new Firebase("https://samu-192.firebaseio.com/veiculos");
            Carro p =new Carro();
            p.setId(UUID.randomUUID().toString());
            p.setPlaca(placa.getText().toString());
            p.setCor(cor.getText().toString());
            p.setModelo(modelo.getText().toString());
            p.setAno(ano.getText().toString());

                 objFirebase.child(p.getId()).setValue(p);
         limpa();

        }
        if (id==R.id.atualizar){
            Firebase objFirebase = new Firebase("https://samu-192.firebaseio.com/veiculos");
            Carro p = new Carro();
            p.setId(CarroSelecionada.getId().toString());
              p.setPlaca(placa.getText().toString());
             p.setAno(ano.getText().toString());
              //p.setId(UUID.randomUUID().toString());
            p.setModelo(modelo.getText().toString());
            p.setCor(cor.getText().toString());
            objFirebase.child(p.getId()).setValue(p);
            limpa();
        }
        if (id==R.id.remover){
            Firebase objFirebase = new Firebase("https://samu-192.firebaseio.com/veiculos");
            Carro p = new Carro();
            p.setId(CarroSelecionada.getId());
            objFirebase.child(p.getId()).removeValue();
        }
        if (id==R.id.pesquisar){


            Intent intent = new Intent(Principal.this,Pesquisa.class);
            startActivity(intent);

        }
        return true;
    }


    private void limpa(){
        placa.setText("");
        modelo.setText("");
        ano.setText("");
        cor.setText("");
    }



    private void   eventoDataBase(){
        Firebase objFirebase = new Firebase("https://samu-192.firebaseio.com/veiculos");

        objFirebase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                CarroList.clear();

                for (DataSnapshot objSnapshot : dataSnapshot.getChildren()) {
                    Carro p = objSnapshot.getValue(Carro.class);

                    CarroList.add(p);
                }
                CarroArrayAdapter = new ArrayAdapter<Carro>(Principal.this,
                        android.R.layout.simple_list_item_1, CarroList);
                lista.setAdapter(CarroArrayAdapter);


            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }


        });
    }

    private void  eventoClicar(){
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CarroSelecionada = (Carro) parent.getItemAtPosition(position);
                ano.setText(CarroSelecionada.getAno());
                cor.setText(CarroSelecionada.getCor());
                modelo.setText(CarroSelecionada.getModelo());
                placa.setText(CarroSelecionada.getPlaca());
            }
        });
    }



}
